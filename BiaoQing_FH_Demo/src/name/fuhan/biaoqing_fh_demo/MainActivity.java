package name.fuhan.biaoqing_fh_demo;

import name.fuhan.biaoqing_fh.BiaoQing_Show;
import name.fuhan.biaoqing_fh.BiaoQing_in;
import name.fuhan.biaoqing_fh.BiaoQing_in.OnSendClickListener;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

	private TextView text1;
	private BiaoQing_Show text2;
	private BiaoQing_in bq_in;
	int requestCode = 123;
	String showNameKey = "name";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		BiaoQing_in.Config
						  .setPath("BiaoQing_default")//指定表情图片目录
				   		  .setMaxTextCount(140)//设置最大字数,默认为140
				   		  .setRowCountAndColumnCount(3, 7)//设置表情选择器的行数和列数,表情图片都是正方形,列数太少会使选择器变得好高
				   		  .setLines(5)//设置输入框为5行
				   		  .setMaxLines(20)//输入框最多输入20行
				   		  .setEnterIsSend(true)//设置可多行输入
				   		  .setShowUsersActivity(ShowUsersActivity.class, requestCode)//设置显示user列表的Activity和返回码
				   		  .setShowUserActivity(ShowUserActivity.class,showNameKey);//设置启动个人详情页的Activity和用于显示在编辑框中的名字的key,本字段应当在显示列表时返回
		
		
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);
		bq_in = $(R.id.bq_in);
		text1 = $(R.id.text1);
		text2 = $(R.id.text2);
		
		bq_in.setOnSendClickListener(new OnSendClickListener() {
			@Override
			public void onSend(String text) {
				text1.setText(text);
				text2.setBiaoQingString(text);
			}
		});
	}
	public <T extends View> T $(int id){
		return (T)findViewById(id);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode==this.requestCode&&resultCode == Activity.RESULT_OK) {
			bq_in.appendAtName(data);//接受来自显示user列表的activity的返回并告诉bq_in往编辑框里追加被@的名字,data中一定要有showNameKey字段
		}
	}
}
