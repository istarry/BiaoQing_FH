package name.fuhan.biaoqing_fh_demo;

import java.util.Iterator;
import java.util.Set;

import name.fuhan.biaoqiang_fh.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ShowUserActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_user);
		TextView text = (TextView) findViewById(R.id.show_user);
		Bundle extras = getIntent().getExtras();
		Set<String> keySet = extras.keySet();
		Iterator<String> iterator = keySet.iterator();
		StringBuilder builder = new StringBuilder();
		while (iterator.hasNext()) {
			String next = iterator.next();
			builder.append(next+"\t=\t"+extras.getString(next)+"\n");
		}
		text.setText(builder);
	}
}
