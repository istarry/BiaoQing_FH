package name.fuhan.biaoqing_fh_demo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import name.fuhan.biaoqiang_fh.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class ShowUsersActivity extends Activity {

	private String json;
	String[] keys = new String[] { "icon", "name", "info" };
	int[] tos = new int[] { R.id.icon, R.id.name, R.id.info };

	{
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		Map<String, String> map = new HashMap<String, String>();
		map.put("icon", "张");
		map.put("name", "张三");
		map.put("info", "ZhangSan");
		list.add(map);
		map = new HashMap<String, String>();
		map.put("icon", "李");
		map.put("name", "李四");
		map.put("info", "LiSi");
		list.add(map);
		map = new HashMap<String, String>();
		map.put("icon", "王");
		map.put("name", "王二麻子");
		map.put("info", "WEMZ");
		list.add(map);

		json = new Gson().toJson(list);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_users_list);
		ListView listView = (ListView) findViewById(R.id.listView1);
		final List<Map<String, String>> data = new Gson().fromJson(json,
				new TypeToken<List<Map<String, String>>>() {
				}.getType());

		listView.setAdapter(new SimpleAdapter(this, data,
				R.layout.user_list_item, keys, tos));

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent intent = getIntent();
				Map<String, String> item = data.get(position);
				for (String key : keys) {
					intent.putExtra(key, item.get(key));
				}
				setResult(RESULT_OK, intent);
				finish();
			}
		});
	}

}
