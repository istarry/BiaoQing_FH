#BiaoQing_FH
![](https://git.oschina.net/fuhan/BiaoQing_FH/raw/master/a.png)
![](https://git.oschina.net/fuhan/BiaoQing_FH/raw/master/b.png)
![](https://git.oschina.net/fuhan/BiaoQing_FH/raw/master/c.png)
![](https://git.oschina.net/fuhan/BiaoQing_FH/raw/master/d.png)
![](https://git.oschina.net/fuhan/BiaoQing_FH/raw/master/e.png)
![](https://git.oschina.net/fuhan/BiaoQing_FH/raw/master/f.png)

这是一个带有表情选择器的输入框

可以自定义的内容

	BiaoQing_in.Config
		  .setPath("BiaoQing_default")//指定表情图片目录
   		  .setMaxTextCount(140)//设置最大字数,默认为140
   		  .setRowCountAndColumnCount(3, 7)//设置表情选择器的行数和列数,表情图片都是正方形,列数太少会使选择器变得好高
   		  .setLines(5)//设置输入框为5行
   		  .setMaxLines(20)//输入框最多输入20行
   		  .setEnterIsSend(true)//设置可多行输入
   		  .setShowUsersActivity(ShowUsersActivity.class, requestCode)//设置显示user列表的Activity和返回码
   		  .setShowUserActivity(ShowUserActivity.class,showNameKey);//设置启动个人详情页的Activity和用于显示在编辑框中的名字的key,本字段应当在显示列表时返回
		
##

图1,图2 : 当呼出表情选择器时自动隐藏软键盘,当软键盘呼出时,隐藏表情选择器

图3 : 当使用单行输入时,软件盘的回车键会显示"发送"字样

图4 : 被@列表选择器

图5 : 被@用户在编辑框和展示区的样子,可点击,点击后到图6

图6 : 点击被@用户的名字时的个人详情展示页

##
#使用方法

在xml中配置输入框

	<name.fuhan.biaoqing_fh.BiaoQing_in
        android:id="@+id/bq_in"
        android:layout_width="match_parent"
        android:layout_height="wrap_content" />
    
这个是展示的text

    <name.fuhan.biaoqing_fh.BiaoQing_Show
        android:id="@+id/text2"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="TextView" />

在Activity中为输入框的发送事件加监听

	bq_in.setOnSendClickListener(new OnSendClickListener() {
		@Override
		public void onSend(Editable editable) {
			text2.setBiaoQingString(editable.toString());
		}
	});


@符号部分已完成

务必请在Activity的onActivityResult()的方法中通知编辑框追加信息
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode==this.requestCode&&resultCode == Activity.RESULT_OK) {
			bq_in.appendAtName(data);//接受来自显示user列表的activity的返回并告诉bq_in往编辑框里追加被@的名字,data中一定要有showNameKey字段
		}
	}



